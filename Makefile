all: compile

compile: clean_rebar_lock
	rebar3 compile
clean_rebar_lock:
	rm -f rebar.lock

clean:
	rebar3 clean
distclean:
	rm -f TEST-*.xml
	rm -rf _build rebar.lock

list-deps:
	rebar3 deps
eunit:
	rebar3 eunit
xref:
	rebar3 xref
dialyzer:
	rebar3 dialyzer

cc: clean compile

ccx: clean compile xref eunit
