%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 20.06.2021
%%% @doc

-module(cfgstarter_utils_test).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([current_reductions/0,
         current_diff_reductions/0, current_diff_reductions/1, current_diff_reductions/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgstarter.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

current_reductions() ->
    lists:foldl(fun(Pid,Acc) -> case erlang:process_info(Pid,reductions) of undefined -> Acc; {_,N} -> N + Acc end end, 0, erlang:processes()).

%% ---
current_diff_reductions() ->
    current_diff_reductions(1000,10).

current_diff_reductions(Sleep) ->
    current_diff_reductions(Sleep,10).

current_diff_reductions(Sleep,Limit) ->
    io:format("current_diff_reductions. self(): ~120p~n",[erlang:self()]),
    Fname = fun(Pid) ->
                    case catch erlang:process_info(Pid,registered_name) of
                        {'EXIT',_} -> [];
                        {registered_name,_}=R -> R;
                        [] -> []
                    end end,
    %
    FReductions = fun(Pid,Acc) ->
                          case erlang:process_info(Pid,reductions) of
                              undefined -> Acc;
                              {_,N} -> [{Pid,N}|Acc]
                          end end,
    R1 = lists:foldl(FReductions, [], erlang:processes()),
    %
    timer:sleep(Sleep),
    %
    FReductions2 = fun(Pid,Acc) ->
                           case erlang:process_info(Pid,reductions) of
                               undefined -> Acc;
                               {_,N} -> [{Pid,N}|Acc]
                           end end,
    R2 = lists:foldl(FReductions2, [], erlang:processes()),
    %
    R3 = lists:filtermap(fun({Pid,N}) ->
                                 case lists:keyfind(Pid,1,R1) of
                                     {_,Nr1} ->
                                         case N-Nr1==0 of
                                             true -> false;
                                             false -> {true,{N-Nr1,Pid,Fname(Pid)}}
                                         end;
                                     false ->
                                         {true,{N,Pid,Fname(Pid)}}
                                 end end, R2),
    R4 = lists:reverse(lists:sort(fun({A,_,_},{B,_,_}) when A=<B -> true; (_,_) -> false end,R3)),
    L = erlang:length(R4),
    {R5,_}=
        case L < Limit of
            true -> lists:split(L,R4);
            false -> lists:split(Limit,R4)
        end,
    R5.