%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 31.01.2021
%%% @doc

-module(cfgstarter_utils).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([get_localcfg_path/0,
         drop_last_subdir/2,
         ensure_dir/1,
         nodename/0, nodename/1,
         get_server_node/0,
         read_json_file/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgstarter.hrl").

-define(MLogPrefix, "Cfgstarter utils").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec get_localcfg_path() -> {ok,LocalCfgPath::binary()} | {error,Reason::binary()}.
%% ---
get_localcfg_path() ->
    case init:get_argument(local_cfg) of
        {ok,[[LocalCfgPath]]} -> {ok,?BU:to_binary(LocalCfgPath)};
        _ ->
            ?LOG('$trace',"Supv. Init argument 'servernode' is not defined! Stop node."),
            {error,<<"local_cfg parameter missed or invalid">>}
    end.

%% ------------------
%% Routines
%% ------------------

%% ---
-spec ensure_dir(DirPath::binary()) -> ok | {error,Reason::file:posix()}.
%% ---
ensure_dir(Path) when is_binary(Path) ->
    PathToCreate = <<Path/binary,"/">>,
    filelib:ensure_dir(PathToCreate).

%% ---
-spec drop_last_subdir(Path::string(),N::integer()) -> NPath::string().
%% ---
drop_last_subdir(Path, 0) -> Path;
drop_last_subdir(Path, N)
  when is_integer(N) ->
    drop_last_subdir(
      lists:droplast(
        lists:reverse(
          lists:dropwhile(fun($/) -> false; (_) -> true end, lists:reverse(Path)))), N-1).

%% ---
-spec nodename() -> NodeName::binary().
%% ---
nodename() ->
    nodename(erlang:node()).

%% ---
-spec nodename(Node::atom()) -> NodeName::binary().
%% ---
nodename(Node) ->
    [NodeName,_] = binary:split(?BU:to_binary(Node),<<"@">>),
    NodeName.

%% ---
-spec get_server_node() -> ServerNode::atom() | no_return().
%% ---
get_server_node() ->
    case init:get_argument(servernode) of
        {ok,[[ServerNode]]} -> ?BU:to_atom_new(ServerNode);
        _ ->
            ?LOG('$trace',"Supv. Init argument 'servernode' is not defined! Stop node."),
            init:stop(1)
    end.

%% ------------------
%% Json files operations
%% ------------------

%% ---
-spec read_json_file(FilePath::binary()) -> {ok,Data::map()} | {error,ErrMap::map()}.
%% ---
read_json_file(FilePath) ->
    ?LOG('$trace',"~p. read_json_file. FilePath:~120p",[?MLogPrefix,FilePath]),
    case file:read_file(FilePath) of
        {ok,DataRaw} ->
            case catch jsx:decode(DataRaw,[return_maps]) of
                {'EXIT',_} -> {error,make_read_error(decode_fail, <<"invalid_json">>)};
                CfgMap -> {ok,CfgMap}
            end;
        {error,Err} -> {error,make_read_error(read_file_error,Err)}
    end.

%% @private
make_read_error(ErrText,ErrReason) ->
    #{error => ErrText, reason => ErrReason}.

%% ====================================================================
%% Internal functions
%% ====================================================================