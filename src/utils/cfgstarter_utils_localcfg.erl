%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 07.08.2021 16:49
%%% @doc

-module(cfgstarter_utils_localcfg).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([save_localcfg_to_env/1,
         get_localcfg_opts/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgstarter.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec save_localcfg_to_env(LocalCfg::map()) -> ok.
%% ---
save_localcfg_to_env(LocalCfg) ->
    FSet = fun(K,V) -> ?BU:set_env('local_cfg',?BU:to_atom_new(K),V) end,
    maps:foreach(FSet,LocalCfg).

%% ---
-spec get_localcfg_opts(Key::atom(),Default::term()) -> Value::term() | Default::term().
%% ---
get_localcfg_opts(Key,Default) ->
    ?BU:get_env('local_cfg',Key,Default).

%% ====================================================================
%% Internal functions
%% ====================================================================