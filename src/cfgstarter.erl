%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 31.01.2021
%%% @doc

-module(cfgstarter).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(application).

-export([start/0, start/2,
         stop/0,stop/1,
         %
         service_ping/1,
         take_cfg/1,
         is_node_load/1
        ]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("cfgstarter.hrl").

%% ====================================================================
%% Public API
%% ====================================================================

%% ---
-spec start() -> ok | {error,Reason::term()}.
%% ---
start() ->
    ?OUTL("Application start.", []),
    add_deps_paths(),
    case load_localcfg() of
        ok ->
            setup_deps_env(),
            Result = application:ensure_all_started(?APP, permanent),
            ?OUTL("Application start. Result:~120p",[Result]),
            case Result of
                {ok, _Started} -> ok;
                {error,_}=Error -> Error
            end;
        {error,_}=Err -> Err
    end.

%% ---
-spec stop() -> ok | {error,Reason::term()}.
%% ---
stop() ->
    application:stop(?APP).

%% ---
-spec service_ping(MapOpts::map()) -> ok.
%% ---
service_ping(MapOpts) when is_map(MapOpts) -> ?LSrv:service_ping(MapOpts).

%% ---
-spec take_cfg(Cfg::map()) -> ok.
%% ---
take_cfg(Cfg) when is_map(Cfg) -> ?LSrv:take_cfg(Cfg).

%% ---
-spec is_node_load(MapOpts::map()) -> Result::boolean().
%% ---
is_node_load(MapOpts) -> ?LSrv:is_node_load(MapOpts).

%% ===================================================================
%% Private
%% ===================================================================

%% ---
start(_Mode, State) ->
    ?OUTL("Application start(Mode, State)", []),
    ?SUPV:start_link(State).

%% ---
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% ---
%% adds app's dependencies paths to code
add_deps_paths() ->
    Path = ?U:drop_last_subdir(code:which(?MODULE), 3),
    DepsPaths = filelib:wildcard(Path++"/*/ebin"),
    AddPathsRes = code:add_pathsa(DepsPaths),
    AddPathsRes.

%% ---
setup_deps_env() ->
    ServerNode = ?U:get_server_node(),
    LogLevel = ?BU:to_atom_new(?LCfgU:get_localcfg_opts(loglevel,'$info')),
    ?BU:set_env(?APPCFGL,server_node,ServerNode),
    ?BU:set_env(?APPBL,max_loglevel,LogLevel).

%% ---
load_localcfg() ->
    case ?U:get_localcfg_path() of
        {ok,LocalCfgPath} ->
            case ?U:read_json_file(LocalCfgPath) of
                {ok,LocalCfg} -> ?LCfgU:save_localcfg_to_env(LocalCfg);
                {error,_ErrMap}=Err ->
                    ?OUTL("load_localcfg. read local_cfg on path (~120p) error:~120p", [LocalCfgPath,_ErrMap]),
                    Err
            end;
        {error,_}=Err -> Err
    end.