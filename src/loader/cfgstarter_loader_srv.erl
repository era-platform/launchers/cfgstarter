%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 31.01.2021
%%% @doc

-module(cfgstarter_loader_srv).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(gen_server).

-export([start_link/1,
         %
         init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3,
         %
         service_ping/1,
         take_cfg/1,
         is_node_load/1
        ]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("cfgstarter.hrl").

-define(StartMsvcCfgRetry, 2000).
-define(StartMsvcRetry, 15000).
-define(CheckSrvPingTout, 10000).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---
start_link(MapOpts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, MapOpts, []).

%% ---
-spec service_ping(MapOpts::map()) -> ok.
%% ---
service_ping(MapOpts) ->
    CallTimeout = maps:get(timeout,MapOpts,5000),
    gen_server:call(?LSrv, {service_ping,MapOpts}, CallTimeout).

%% ---
-spec take_cfg(Cfg::map()) -> ok.
%% ---
take_cfg(Cfg) -> gen_server:cast(?LSrv, {take_cfg,Cfg}).

%% ---
-spec is_node_load(MapOpts::map()) -> Result::boolean() | no_return().
%% ---
is_node_load(MapOpts) ->
    CallTimeout = maps:get(timeout,MapOpts,5000),
    gen_server:call(?LSrv, {is_node_load,MapOpts}, CallTimeout).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(MapOpts) ->
    Ref = erlang:make_ref(),
    ServerNode = maps:get(servernode,MapOpts),
    State = #lsrvstate{servernode=ServerNode,
                       cfg_data=#{},
                       last_srv_ping_ts=?BU:timestamp(),
                       ref=Ref},
    erlang:send_after(0,self(),{start_load,Ref}),
    erlang:send_after(1000,self(),{check_server_ping,Ref}),
    ?OUTL("Inited. Local: ~p (~p).", [?MODULE, self()]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------
handle_call({service_ping,MapOpts}, _From, State) ->
    ?LOG('$trace',"Loader. rcv service_ping"),
    erlang:spawn(fun() -> handle_service_ping(MapOpts,State) end),
    Reply = ok,
    State1 = State#lsrvstate{last_srv_ping_ts=?BU:timestamp()},
    {reply, Reply, State1};

handle_call({is_node_load,_MapOpts}, _From, State) ->
    ?LOG('$trace',"Loader. rcv is_node_load"),
    Reply = true,
    {reply, Reply, State};

%% default
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({take_cfg,Cfg}, #lsrvstate{cfg_data=CD,load_cfg_ref=undefined}=State) ->
    % @todo в одном процессе обрабатывается и обновление конфигурации (синхронно) и загрузка микросервисов (приложений, синхронно) и анализ пингов от сервера, в определенный момент это может вызвать проблемы, поэтому как решение - выделить обработчик пингов от сервера в отдельный процесс. т.к. конфигурация компилится в бимку на диске, то передавать конфиг в новый процесс не нужно, поэтому это возможно безболезненно.
    ?LOG('$trace',"Loader. rcv take_cfg"),
    FLoad = fun() ->
                    Result = load_cfg(Cfg),
                    gen_server:cast(?LSrv,{load_cfg_result,Result})
            end,
    PidRef = erlang:spawn_monitor(FLoad),
    ?LOG('$trace',"Loader. rcv take_cfg. worker:~120p",[PidRef]),
    State1 = State#lsrvstate{cfg_data=CD#{cfg_hash => erlang:phash2(Cfg)},
                             load_cfg_ref=PidRef},
    {noreply, State1};

handle_cast({load_cfg_result,ok}, State) ->
    ?LOG('$trace',"Loader. load_cfg_result ok"),
    notifying_msvc_at_cfg_change(State),
    State1 = State#lsrvstate{load_cfg_ref=undefined},
    {noreply, State1};
handle_cast({load_cfg_result,{error,Reason}}, #lsrvstate{cfg_data=CD}=State) ->
    ?LOG('$trace',"Loader. load_cfg_result error:~120p",[Reason]),
    CD1 = maps:without([cfg_hash],CD),
    State1 = State#lsrvstate{cfg_data=CD1,load_cfg_ref=undefined},
    {noreply, State1};

%% default
handle_cast(_Request, State) ->
    ?OUTL("~p. RCV unknown CAST:~p",[?ServiceName,_Request]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% start_load
handle_info({start_load,Ref}, #lsrvstate{cfg_data=CD,is_msvcs_started=IsMsvcsStarted,ref=Ref}=State)
  when is_integer(map_get(cfg_hash,CD)) orelse IsMsvcsStarted==false ->
    ?LOG('$trace',"Loader. start_load. cfg is loaded."),
    #lsrvstate{servernode=SrvNode}=State,
    TryStartRes = try_start_node_msvcs(),
    ?LOG('$trace',"Loader. try_start_node_msvc result:~120p",[TryStartRes]),
    FRetry = fun(Timeout) -> erlang:send_after(Timeout,self(),{start_load,Ref}) end,
    case TryStartRes of
        ok -> ok;
        {error,cfg_not_loaded} ->
            request_get_cfg(SrvNode),
            FRetry(?StartMsvcRetry);
        {error,start_app_crash} -> FRetry(?StartMsvcRetry);
        {error,_} -> FRetry(?StartMsvcRetry);
        {retry_after,Timeout,_} -> FRetry(Timeout)
    end,
    State1 = case TryStartRes==ok of true -> State#lsrvstate{is_msvcs_started=true}; false -> State end,
    {noreply, State1};
%% retry start_load when cfg is not loaded yet or msvcs is not loaded
handle_info({start_load,Ref}, #lsrvstate{ref=Ref}=State) ->
    ?LOG('$trace',"Loader. start_load. cfg is not loaded yet or node microservices is not started."),
    erlang:send_after(?StartMsvcCfgRetry,self(),{start_load,Ref}),
    {noreply,State};

%% load worker down
handle_info({'DOWN',MonRef,process,Pid,_Reason}, #lsrvstate{cfg_data=CD,load_cfg_ref={Pid,MonRef}}=State) ->
    ?LOG('$trace',"Loader. Rcv down from load cfg worker (~120p). Error:~120p",[Pid,_Reason]),
    CD1 = maps:without([cfg_hash],CD),
    State1 = State#lsrvstate{cfg_data=CD1,load_cfg_ref=undefined},
    {noreply, State1};

%% check ping from server
handle_info({check_server_ping,Ref}, #lsrvstate{ref=Ref}=State) ->
    ?LOG('$trace',"Loader. Rcv check_server_ping"),
    #lsrvstate{cfg_data=CD,last_srv_ping_ts=PingTs}=State,
    IsExistsCfg = erlang:is_integer(maps:get(cfg_hash,CD,undef)),
    erlang:spawn(fun() -> check_exists_server_pings(PingTs,IsExistsCfg) end),
    erlang:send_after(?CheckSrvPingTout,self(),{check_server_ping,Ref}),
    {noreply,State};

%% default
handle_info(_Info, State) ->
    ?OUTL("~p. RCV unknown INFO:~p",[?ServiceName,_Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------
%% ping from cfgshell
%% ------------------------------

%% ---
handle_service_ping(MapOpts,#lsrvstate{servernode=SrvNode,cfg_data=CfgD}) ->
    %?LOG('$trace',"Loader. start handle_service_ping."),
    StateCfgH = maps:get(cfg_hash,CfgD,undef),
    CfgH = maps:get(cfg_hash,MapOpts),
    IsEqHashs = StateCfgH==CfgH,
    %?LOG('$trace',"Loader. start handle_service_ping. compare cfg hash result:~120p",[IsEqHashs]),
    case IsEqHashs of
        true -> ok;
        false -> request_get_cfg(SrvNode)
    end.

%% ---
request_get_cfg(SrvNode) ->
    ?LOG('$trace',"Loader. request_get_cfg. send request to:~120p",[SrvNode]),
    NodeName = ?U:nodename(),
    ?BRpc:cast(SrvNode, ?AppCfgShell, node_request, [get_cfg,NodeName]).

%% ---
load_cfg(CfgMap) ->
    ?LOG('$trace',"Loader. load_cfg. start load cfg"),
    case ?CfgL:load_cfg(CfgMap) of
        {ok,_ResMap} -> ok;
        {error,_}=Err1 -> Err1
    end.

%% ---
check_exists_server_pings(PingTs,IsExistsCfg) ->
    SecAfterLastPing = (?BU:timestamp()-PingTs)/1000,
    FStop = fun() ->
                    ?OUTL("Loader. check_exists_server_pings. ping from server timeout when exists cfg is '~p'. The node stops",[IsExistsCfg]),
                    ?LOG('$trace',"Loader. check_exists_server_pings. ping from server timeout when exists cfg is '~p'. The node stops",[IsExistsCfg]),
                    timer:sleep(1000),
                    init:stop()
            end,
    case {IsExistsCfg,SecAfterLastPing} of
        {false,Sec1} when Sec1 > 60 -> FStop();
        {true, Sec2} when Sec2 > 30 -> FStop();
        {_,_} -> ok
    end.

%% ------------------------------
%% start one application on node
%% ------------------------------

%% ---
try_start_node_msvcs() ->
    case get_msvcs_to_start() of
        {ok, MsvcsInfo} -> try_start_node_msvc(MsvcsInfo);
        {error,_}=Err -> Err
    end.

%% @private
get_msvcs_to_start() ->
    case catch ?CfgLF:get_current_node_info() of
        {error, not_loaded} -> {error, cfg_not_loaded}; % from cfglib throw
        NodeInfo when is_map(NodeInfo) ->
            MsvcsInfo = maps:get(<<"microservices">>,NodeInfo),
            ?LOG('$trace',"Loader. get_msvcs_to_start. current node msvcinfos: ~120p",[MsvcsInfo]),
            {ok,MsvcsInfo}
    end.

%% @private
try_start_node_msvc([]) -> ok;
try_start_node_msvc([MsvcInfo|T]) ->
    case try_start_node_one_msvc(MsvcInfo) of
        ok -> try_start_node_msvc(T);
        ErrOrRetry -> ErrOrRetry
    end.

%% @private
try_start_node_one_msvc(MsvcInfo) when is_map(MsvcInfo) ->
    ?LOG('$trace',"Loader. start try_start_node_msvc. MsvcInfo: ~120p",[MsvcInfo]),
    %
    [AppName,StartPoint,ApplicationPath] = get_app_descriptor_info(MsvcInfo),
    ?LOG('$trace',"Loader. try_start_node_msvc. StartPoint: ~120p~nApplicationPath:~120p~nAppName:~120p",[StartPoint,ApplicationPath,AppName]),
    add_app_codepaths(ApplicationPath),
    set_application_env(AppName,MsvcInfo),
    %
    FDefStart = fun() ->
                        FExec = fun() ->
                                        case application:ensure_all_started(AppName) of
                                            {ok,_} -> ok;
                                            {error,_}=Err -> Err
                                        end
                                end,
                        FErr = fun() ->
                                       ?LOG('$trace',"Loader. try_start_node_msvc. default start fun timeout. Stop node.",[]),
                                       ?BU:stop_node()
                               end,
                        ?BU:exec_fun_timered(FExec,FErr,3000)
                end,
    exec_msvc_point(StartPoint,FDefStart).

%% @private
get_app_descriptor_info(MsvcInfo) ->
    MsvcType = maps:get(type,MsvcInfo),
    ?LOG('$trace',"Loader. get_app_descriptor_info. MsvcType:~120p",[MsvcType]),
    [AppName,_StartPoint,_AppPath]=Res = ?CfgL:get_msvc_descriptor_info(MsvcType,[name,start_point,app_path]),
    ?LOG('$trace',"Loader. get_app_descriptor_info. for app '~p'~nRes: ~120p",[AppName,Res]),
    Res.

%% @private
add_app_codepaths(ApplicationPath) ->
    ProjectPath = ?BU:drop_last_subdir(?BU:to_unicode_list(ApplicationPath),1),
    ?LOG('$trace',"Loader. add_app_codepaths. ProjectPath:~120p",[ProjectPath]),
    FilesPathsRaw = filelib:wildcard(ProjectPath++"/*/ebin"),
    ?LOG('$trace',"Loader. add_app_codepaths. FilesPathsRaw:~120p",[FilesPathsRaw]),
    FilePaths = filter_exests_codepaths(FilesPathsRaw),
    ?LOG('$trace',"Loader. add_app_codepaths. paths to add FilePaths:~120p",[FilePaths]),
    AddPRes = code:add_pathsa(FilePaths),
    ?LOG('$trace',"Loader. add_app_codepaths. add paths result:~120p",[AddPRes]),
    ok.

%% private
%% if last 2 parts (app_name/ebin) of path for add already exists in code paths (may be in another app) it should be skipped
filter_exests_codepaths(FilesPathsRaw) ->
    FLast2Path = fun(Path) -> [A,B|_] = lists:reverse(filename:split(Path)), [B,A] end,
    CodeLastPaths = [FLast2Path(P) || P <- code:get_path(), erlang:length(filename:split(P))>2],
    FFilter = fun(Path) -> not lists:member(FLast2Path(Path),CodeLastPaths) end,
    lists:filter(FFilter,FilesPathsRaw).

%% ------------------------------
%% refresh application
%% ------------------------------

%% ---
notifying_msvc_at_cfg_change(#lsrvstate{is_msvcs_started=false}) ->
    ?LOG('$trace',"Loader. notifying_msvc_at_cfg_change. apps is not started yet!"),
    ok;
notifying_msvc_at_cfg_change(_State) ->
    NodeInfo = ?CfgLF:get_current_node_info(),
    MsvcsInfo = maps:get(<<"microservices">>,NodeInfo),
    lists:foreach(fun(MsvcInfo) -> notify_one_msvc(MsvcInfo) end, MsvcsInfo).

%% @private
notify_one_msvc(MsvcInfo) ->
    MsvcType = maps:get(type,MsvcInfo),
    ?LOG('$trace',"Loader. refresh_app_env. MsvcType:~120p",[MsvcType]),
    AppName = ?CfgL:get_msvc_descriptor_info(MsvcType,name),
    ?LOG('$trace',"Loader. refresh_app_env. AppName:~120p",[AppName]),
    notify_to_update_params(AppName,MsvcType,MsvcInfo),
    notify_to_update_cfg(MsvcType),
    ok.

%% @private
notify_to_update_params(AppName,MsvcType,MsvcInfo) ->
    case refresh_application_env(AppName,MsvcInfo) of
        not_changed ->
            ?LOG('$trace',"Loader. notify_to_update_params. refresh_application_env. params not_changed"),
            ok;
        ok ->
            ?LOG('$trace',"Loader. notify_to_update_params. refresh_application_env. params changed"),
            case catch ?CfgL:get_msvc_descriptor_info(MsvcType,update_params_point) of
                undefined ->
                    ?LOG('$trace',"Loader. notify_to_update_params. app update_params_point not found!"),
                    ok;
                [_,_,_]=UpdatePoint -> exec_msvc_point(UpdatePoint,fun() -> ok end);
                UpPoint ->
                    ?LOG('$trace',"Loader. notify_to_update_params. app invalid update_params_point format. update skipped!~nUpdatePoint: ~120p",[UpPoint]),
                    ok
            end
    end.

%% @private
refresh_application_env(AppName,MsvcInfoRaw) ->
    MsvcInfo = maps:to_list(format_application_params(MsvcInfoRaw)),
    AppNameAtom = ?BU:to_atom_new(AppName),
    case ?BU:get_all_env(AppNameAtom) of
        [] when length(MsvcInfo)==0 -> not_changed;
        [] ->
            ?LOG('$trace',"Loader. refresh_application_env.App: ~120p~nOld params:~120p~nNew params:~120p",[AppName,[],MsvcInfo]),
            set_application_env(AppName,MsvcInfo),
            ok;
        [_|_]=OldMsvcInfo ->
            ?LOG('$trace',"Loader. refresh_application_env.App: ~120p~nOld params:~120p~nNew params:~120p",[AppName,OldMsvcInfo,MsvcInfo]),
            AddParams = MsvcInfo -- OldMsvcInfo,
            DelParams = OldMsvcInfo -- MsvcInfo,
            case {AddParams,DelParams} of
                {[],[]} -> not_changed;
                {[],_} -> unset_application_env(AppName,DelParams),ok;
                {_,[]} -> set_application_env(AppName,AddParams), ok;
                {_,_} ->
                    set_application_env(AppName,AddParams),
                    unset_application_env(AppName,DelParams),
                    ok
            end
    end.

%% @private
notify_to_update_cfg(MsvcType) ->
    ?LOG('$trace',"Loader. notify_to_update_cfg"),
    case catch ?CfgL:get_msvc_descriptor_info(MsvcType,update_cfg_point) of
        undefined ->
            ?LOG('$trace',"Loader. notify_to_update_cfg. app update_cfg_point not found!"),
            ok;
        [_,_,_]=UpdateCfgPoint -> exec_msvc_point(UpdateCfgPoint,fun() -> ok end);
        UpCfgPoint ->
            ?LOG('$trace',"Loader. notify_to_update_cfg. app invalid update_cfg_point format. update skipped!~nUpdateCfgPoint: ~120p",[UpCfgPoint]),
            ok
    end.

%% ------------------------------
%% utilites
%% ------------------------------

%% ---
set_application_env(App,Params) when is_map(Params) ->
    AppAtom = ?BU:to_atom_new(App),
    FSet = fun(K,V) -> ?BU:set_env(AppAtom,?BU:to_atom_new(K),V) end,
    maps:foreach(FSet,Params);
set_application_env(App,Params) when is_list(Params) ->
    AppAtom = ?BU:to_atom_new(App),
    FSet = fun({K,V}) -> ?BU:set_env(AppAtom,?BU:to_atom_new(K),V) end,
    lists:foreach(FSet,Params).

%% ---
unset_application_env(App,Params) ->
    AppAtom = ?BU:to_atom_new(App),
    FUnSet = fun({K,_}) -> ?BU:unset_env(AppAtom,?BU:to_atom_new(K)) end,
    lists:foreach(FUnSet,Params).

%% ---
format_application_params(Params) ->
    FFormat = fun(K,V,Acc) -> Acc#{?BU:to_atom_new(K) => V} end,
    maps:fold(FFormat,#{},Params).

%% ---
exec_msvc_point(undefined,FDefault) ->
    ?LOG('$trace',"Loader. exec_msvc_point. Msvc point is undefined",[]),
    FDefault();
exec_msvc_point(MsvcPoint,FDefault) ->
    [M0,F0,A]=MsvcPoint,
    [M,F] = [?BU:to_atom_new(M0),?BU:to_atom_new(F0)],
    case ?BU:function_exported(M,F,erlang:length(A)) of
        true ->
            FExec = fun() ->
                            case catch erlang:apply(M,F,A) of
                                {'EXIT',_Err1} ->
                                    ?LOG('$crash',"Loader. exec_msvc_point. apply point mfa crash:~120p",[_Err1]),
                                    {error, start_app_crash};
                                ok -> ok;
                                {ok,_} -> ok;
                                {error,_}=Err -> Err;
                                {retry_after,_Timeout,_Reason}=Retry -> Retry
                            end
                    end,
            FErr = fun() ->
                           ?LOG('$trace',"Loader. exec_msvc_point. apply point timeout. Stop node. Point {~120p,~120p,~120p}",[M,F,A]),
                           ?BU:stop_node()
                   end,
            ?BU:exec_fun_timered(FExec,FErr,3000);
        false -> FDefault()
    end.